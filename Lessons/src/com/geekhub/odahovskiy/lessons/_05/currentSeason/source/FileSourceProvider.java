package com.geekhub.odahovskiy.lessons._05.currentSeason.source;

import javafx.scene.shape.Path;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File readFile = new File(pathToSource);
        return readFile.isFile() && readFile.canRead() && readFile.exists();
    }

    @Override
    public String load(String pathToSource) throws IOException {

        BufferedReader reader = Files.newBufferedReader(Paths.get(pathToSource), StandardCharsets.UTF_8);

        return reader.lines().map(line -> line).collect(Collectors.joining("\n"));
    }
}
