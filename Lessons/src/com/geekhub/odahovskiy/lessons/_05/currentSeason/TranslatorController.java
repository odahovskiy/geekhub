package com.geekhub.odahovskiy.lessons._05.currentSeason;


import com.geekhub.odahovskiy.lessons._05.currentSeason.source.SourceLoader;
import com.geekhub.odahovskiy.lessons._05.currentSeason.source.URLSourceProvider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        System.out.println("Write path to file or exit to stop app.");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();

        while(!"exit".equals(command)) {
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);
                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
            }catch (UnknownHostException e){
                System.out.println("unable to connect: " + command);
            }catch (FileNotFoundException e){
                System.out.println("file not found: " + command);
            }catch (IOException e){
                System.out.println(e.getMessage());
            }
            command = scanner.nextLine();
        }
    }
}
