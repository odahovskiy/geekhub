package com.geekhub.odahovskiy.lessons._05.lastSeason.task2;

import java.io.IOException;

/**
 * Created by Vova Odahovskiy on 10.11.14.
 */
public class StartServer {
    public static void main(String[] args) {
        //def port 8881
        Server server = new Server();
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
