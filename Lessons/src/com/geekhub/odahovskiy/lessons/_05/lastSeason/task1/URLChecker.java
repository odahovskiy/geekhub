package com.geekhub.odahovskiy.lessons._05.lastSeason.task1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

/**
 * Created by Vova Odahovskiy on 10.11.14.
 */
public class URLChecker {

    /**
     * Main method that check all links that available by URL
     * @param url
     * @return Map<Integer,List<URL>> - Integer - status code
     * @throws IOException
     */
    public static Map<Integer,Set<URL>> check(String url) throws IOException {
        URL workUrl = new URL(url);
        HttpURLConnection httpURLConnection = openConnection(workUrl);
        System.out.printf("Open connection on %s\n", url);
        Map<Integer,Set<URL>> urlList = getURLsFromContent(httpURLConnection);
        return urlList;
    }


    private static Map<Integer,Set<URL>> getURLsFromContent(HttpURLConnection connection) throws IOException {
        System.out.printf("Getting links from %s\n", connection.getURL().toString());
        System.out.printf("Wait..\n");

        Map<Integer, Set<URL>> urls = new TreeMap<>();
        String url = connection.getURL().toString();

        Document document = Jsoup.connect(url).get();//get content by from url
        Elements links = document.select("a[href]");//get all link tags
        for (Element link : links) {
            URL linkUrl = new URL(link.attr("abs:href"));
            try{
                int code = getStatusCode(linkUrl.toString());
                putUrl(urls, code, linkUrl);
            }catch (IOException ex) {
                putUrl(urls, 404, linkUrl);// if page not exist put with code 404
            }
        }

        return urls;
    }

    private static void putUrl(Map<Integer, Set<URL>> urls, int code, URL url) {
        Set<URL> urlList = (null == urls.get(code)) ? new HashSet<URL>(Collections.EMPTY_SET)
                                                     : urls.get(code);
        urlList.add(url);
        urls.put(code, urlList);
    }

    private static int getStatusCode(String link) throws IOException {
        URL url = new URL(link);
        HttpURLConnection connection = openConnection(url);
        return connection.getResponseCode();
    }


    private static HttpURLConnection openConnection(URL link) throws IOException {
        HttpURLConnection http = null;
        try {
           http = (HttpURLConnection)link.openConnection();
           int code = http.getResponseCode();
        } catch (Exception e) {
            throw new IOException("Bad url: " + e.getMessage());
        }
        return http;
    }
}
