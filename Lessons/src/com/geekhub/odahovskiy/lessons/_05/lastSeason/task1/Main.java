package com.geekhub.odahovskiy.lessons._05.lastSeason.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Vova Odahovskiy on 10.11.14.
 */
public class Main {
    public static void main(String[] args) {
        String url = InputUrl();
        try {
            Map<Integer,Set<URL>> urls  = URLChecker.check(url);
            print(urls);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    private static String InputUrl() {
        String url  = "http://cdu.edu.ua/";

        boolean tryInput = true;
        do{
            try (BufferedReader reader = new BufferedReader(
                                            new InputStreamReader(System.in))) {

                System.out.printf("Please input full url, like http://geekhub.ck.ua\n");
                System.out.printf("Input: ");
                String inputValue = reader.readLine();
                url = inputValue;
                tryInput = false;
            }catch (Exception ex) {
                System.out.printf(ex.toString() + "\n");
            }
        }while (tryInput);
        return url;
    }

    private static void print(Map<Integer, Set<URL>> urls) {
        for (Map.Entry<Integer, Set<URL>> statusCode : urls.entrySet()) {
            for (URL url : statusCode.getValue()) {
                int code = statusCode.getKey();
                String link = url.toString();
                System.out.printf("Status code %d link %s\n", code, link);
            }
        }
    }
}
