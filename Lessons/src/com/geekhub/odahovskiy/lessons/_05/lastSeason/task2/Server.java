package com.geekhub.odahovskiy.lessons._05.lastSeason.task2;

import java.io.*;
import java.net.Socket;

/**
 * Created by Vova Odahovskiy on 10.11.14.
 */
public class Server {
    private Integer port;
    private java.net.ServerSocket serverSocket;

    public Server(){
        port = 8881;
    }

    public Server(Integer port){
        this.port = port;
    }


    public int getPort() {
        return port;
    }

    public void start() throws IOException {

        if (null == port)
            throw new NullPointerException("Port cannot be null");

        if ( (1025 > port) || (65535 < port) )
            throw new IllegalArgumentException("Port should be in 1025..65535");

        try {
            //build server socket
            serverSocket = new java.net.ServerSocket(port);
            workWithClients();
        }catch(Exception x) {
            System.out.println(x.getMessage());
        }
    }

    private void workWithClients() throws IOException {
        System.out.println("Waiting for a client..");

        //waiting for client
        Socket socket = serverSocket.accept();
        System.out.println("Got a client!");

        //input \ output streams
        InputStream sin = socket.getInputStream();
        OutputStream sout = socket.getOutputStream();

        //converting streams
        DataInputStream in = new DataInputStream(sin);
        DataOutputStream out = new DataOutputStream(sout);

        String line = null;
        while(true) {
            line = in.readUTF();//read message from client
            System.out.printf("Message from client: %s\n",line);
            out.writeUTF(String.valueOf(line.charAt(0)));//send back 1st letter
            out.flush();//say stream to stop sending
        }
    }
}
