package com.geekhub.odahovskiy.lessons._05.lastSeason.task2;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Vova Odahovskiy on 10.11.14.
 */
public class Client {

    private int serverPort;
    private String host;

    public Client(int serverPort, String host){
        this.serverPort = serverPort;
        this.host = host;
        workWithServer();
    }

    public Client(){
        serverPort = 6666;
        host = "127.0.0.1";
        workWithServer();
    }

    private void workWithServer() {
        try {
            //build socket
            InetAddress ipAddress = InetAddress.getByName(host);
            Socket socket = new Socket(ipAddress, serverPort);

            //opening streams
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            //converting streams
            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            //reader from board
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;

            while (true) {
                System.out.println("Input Text:");
                line = keyboard.readLine();//read client text
                out.writeUTF(line);//sending text from client to server
                out.flush();//say client to stop sending
                line = in.readUTF();//reading text from server
                System.out.printf("Message from server: %s\n ", line);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }


}
