package com.geekhub.odahovskiy.lessons._06.adapters;

import com.geekhub.odahovskiy.lessons._06.adapters.JsonDataAdapter;
import com.geekhub.odahovskiy.lessons._06.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        c.stream().forEach(object -> jsonArray.put(JsonSerializer.serialize(object)));
        return jsonArray;
    }
}
