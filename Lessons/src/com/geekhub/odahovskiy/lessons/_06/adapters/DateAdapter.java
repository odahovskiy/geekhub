package com.geekhub.odahovskiy.lessons._06.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Converts object of type java.util.Date to String by using dd/MM/yyyy format
 */
public class DateAdapter implements JsonDataAdapter<Date> {
    @Override
    public Object toJson(Date date) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = format.format(date);
        return "("+formattedDate+")";
    }
}
