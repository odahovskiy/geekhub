package com.geekhub.odahovskiy.lessons._08.currentSeason.objects;

import java.util.Date;

public class User extends Entity {

    private String name = null;
    private Integer age = null;
    private Boolean admin = null;
    private Double balance = null;

    @Ignore
    private Date creationDate;

    public User() {
        this.creationDate = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", admin=" + admin +
                ", balance=" + balance +
                '}';
    }
}
