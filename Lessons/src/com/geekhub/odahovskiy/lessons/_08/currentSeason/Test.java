package com.geekhub.odahovskiy.lessons._08.currentSeason;


import com.geekhub.odahovskiy.lessons._08.currentSeason.objects.Address;
import com.geekhub.odahovskiy.lessons._08.currentSeason.objects.Cat;
import com.geekhub.odahovskiy.lessons._08.currentSeason.objects.User;
import com.geekhub.odahovskiy.lessons._08.currentSeason.storage.DatabaseStorage;
import com.geekhub.odahovskiy.lessons._08.currentSeason.storage.Storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        Connection connection = createConnection("root", "11223354aa", "geekdb");
        Storage storage = new DatabaseStorage(connection);

        test1(storage);
        test2(storage);

        connection.close();
    }

    private static void test2(Storage storage) throws Exception {
        User user = new User();
        user.setBalance(21.3);
        user.setAdmin(false);
        user.setAge(13);
        storage.save(user);

        Address adress = new Address();
        adress.setStreet("Obukhov");
        adress.setStreetNumber(113);
        adress.setHouseNumber(13);
        storage.save(adress);

        Address adress2 = storage.get(Address.class, adress.getId());

        if (!adress.getStreet().equals(adress2.getStreet())) throw new Exception("Address should be equals");

        adress2.setPhoneNumber("093 - 93 - 777 - 14");
        storage.save(adress2);


    }

    private static void test1(Storage storage) throws Exception {
        List<Cat> cats = storage.list(Cat.class);
        for (Cat cat : cats) {
            storage.delete(cat);
        }
        cats = storage.list(Cat.class);
        if (!cats.isEmpty()) throw new Exception("Cats should not be in database!");

        for(int i = 1; i <= 20; i++) {
            Cat cat = new Cat();
            cat.setName("cat" + i);
            cat.setAge(i);
            storage.save(cat);
        }

        cats = storage.list(Cat.class);
        if (cats.size() != 20) throw new Exception("Number of cats in storage should be 20!");

        User user = new User();
        user.setAdmin(true);
        user.setAge(23);
        user.setName("Victor");
        user.setBalance(22.23);
        storage.save(user);

        User user1 = storage.get(User.class, user.getId());
        if (!user1.getName().equals(user.getName())) throw new Exception("Users should be equals!");

        user.setAdmin(false);
        storage.save(user);

        System.out.println("USER ID " +user.getId());
        User user2 = storage.get(User.class, user.getId());
        if (!user.getAdmin().equals(user2.getAdmin())) throw new Exception("Users should be updated!");

        storage.delete(user1);

        User user3 = storage.get(User.class, user.getId());

        if (user3 != null) throw new Exception("User should be deleted!");
    }

    private static Connection createConnection(String login, String password, String dbName) throws Exception {
        String host = "localhost";
        String url = "jdbc:mysql://" + host + ":3306/" + dbName;
        String driver = "com.mysql.jdbc.Driver";
        Connection connection = null;
        try {
            Class.forName(driver);//reg driver
            connection = DriverManager.getConnection(url, login, password);
            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new Exception(e);
        }finally {
            if (null != connection){
                System.out.println("Connection to "+dbName+" success.");
            }
        }
        return null;
    }
}
