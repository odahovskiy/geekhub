package com.geekhub.odahovskiy.lessons._08.currentSeason.storage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Vova Odahovskiy on 30.11.2014.
 * Class contain methods that generate and return queries.
 */
class QueryBuilder{

    /**
     * Update query generate method
     * @param data map that contains entity keys and values
     * @param tableName equals entity class name
     * @return prepared query
     */
    public String generateUpdateQuery(Map<String, Object> data,String tableName) {
        StringBuilder queryBuilder = new StringBuilder();
        String splitSymbol = "::";//delimiter for keys
        queryBuilder.append("UPDATE ").append(tableName).append(" SET ");

        String keys = data.keySet().stream()
                .filter(key -> !"id".equals(key))
                .collect(Collectors.joining(splitSymbol));

        String values = data.values().stream()
                .filter(value -> null != value)
                .map(Object::toString).collect(Collectors.joining(splitSymbol));

        for (int i = 0; i < keys.split(splitSymbol).length; i++) {
            String key = keys.split(splitSymbol)[i];
            String value = "'" + values.split(splitSymbol)[i] + "'";
            queryBuilder.append(key).append("=").append(value).append(",");
        }
        queryBuilder.delete(queryBuilder.lastIndexOf(","), queryBuilder.length());
        queryBuilder.append(" WHERE id = '").append(data.get("id")).append("'");
        return queryBuilder.toString();
    }

    /**
     * Insert query generate method
     * @param data map that contains entity keys and values
     * @param tableName equals entity class name
     * @return prepared query
     */
    public String generateInsertQuery(Map<String, Object> data,String tableName) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("INSERT INTO ").append(tableName).append("(");

        String keys = data.keySet().stream()
                .filter(key -> null != data.get(key))
                .collect(Collectors.joining(","));
        queryBuilder.append(keys).append(")").append(" VALUES ('");

        String values = data.values().stream()
                .filter(value -> null != value)
                .map(Object::toString).collect(Collectors.joining("','"));
        queryBuilder.append(values).append("')");

        return queryBuilder.toString();
    }

    /**
     * @param resultSet contain key
     * @return primary key of last insert entity
     * @throws SQLException
     */
    public int getInsertedKey(ResultSet resultSet) throws SQLException {
        int id = 0;
        if (resultSet.next()) {
            id = resultSet.getInt(1);
        }
        return id;
    }
}
