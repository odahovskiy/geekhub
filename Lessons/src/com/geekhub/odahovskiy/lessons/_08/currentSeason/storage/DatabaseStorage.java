package com.geekhub.odahovskiy.lessons._08.currentSeason.storage;

import com.geekhub.odahovskiy.lessons._08.currentSeason.objects.Entity;
import com.geekhub.odahovskiy.lessons._08.currentSeason.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link com.geekhub.odahovskiy.lessons._08.currentSeason.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM "+ clazz.getSimpleName().toLowerCase();
        List<T> result = new ArrayList<>(Collections.EMPTY_LIST);
        try(Statement statement = connection.createStatement()) {
            result = extractResult(clazz, statement.executeQuery(sql));
        }finally {
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        boolean result = true;
        Integer id;
        String table = entity.getClass().getSimpleName().toLowerCase();
        String sql = "DELETE FROM " + table + " WHERE id =?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            id = entity.getId();
            statement.setInt(1, id);
            statement.execute();
        }catch (NullPointerException ex){
            result = false;
        }
        finally {
            return result;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        QueryBuilder queryBuilder = new QueryBuilder();
        String tableName = entity.getClass().getSimpleName().toLowerCase();
        String sql;
        if (entity.isNew()) {
            sql = queryBuilder.generateInsertQuery(data, tableName);
        } else {
            sql = queryBuilder.generateUpdateQuery(data, tableName);
        }

       try(Statement statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)) {
           statement.executeUpdate(sql);
           if (entity.isNew()) {
               int id = queryBuilder.getInsertedKey(statement.getGeneratedKeys());
               entity.setId(id);
           }
       }catch (Exception sqlEx){
           System.out.print("Could not insert query");
       }finally {
           System.out.println(sql);//show sql query in console
       }


    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> preparedEntity = new LinkedHashMap<>(Collections.EMPTY_MAP);
        Objects.requireNonNull(entity);
        Class clazz = entity.getClass();

        List<Field> openFields = getDeclaredFields(clazz, Collections.EMPTY_LIST);

        for (Field field : openFields) {
            field.setAccessible(true);
            preparedEntity.put(field.getName(), !field.getType().equals(Boolean.class)
                    ? field.get(entity)
                    : (field.get(entity).toString().equals("true")) ? 1 : 0);
            field.setAccessible(false);
        }

        return preparedEntity;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> resultList = new ArrayList<>();

        List<Field> openFields = getDeclaredFields(clazz, Collections.EMPTY_LIST);

        while (resultset.next()){
            T object = clazz.newInstance();
            for (Field field : openFields) {
                field.setAccessible(true);
                field.set(object, resultset.getObject(field.getName()));
                field.setAccessible(false);
            }
            resultList.add(object);

        }
            return resultList;
    }


    private <T extends Entity> List<Field> getDeclaredFields(Class<T> clazz, List<Field> list){
        List<Field> openFields = new ArrayList<>(list);

        openFields.addAll(Arrays.asList(clazz.getDeclaredFields())
                .stream().filter(field -> !field.isAnnotationPresent(Ignore.class))
                .collect(Collectors.toList()));

        if (!Object.class.getName().equals(clazz.getSuperclass().getName())){
            Class superClass = clazz.getSuperclass();
            return getDeclaredFields(superClass, openFields);
        }
        return openFields;
    }
}
