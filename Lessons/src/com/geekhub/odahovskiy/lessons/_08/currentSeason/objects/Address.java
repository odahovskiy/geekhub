package com.geekhub.odahovskiy.lessons._08.currentSeason.objects;


/**
 * Created by Vova Odahovskiy on 30.11.2014.
 * Insert query if you want to test
 * query of table located down of file.
 */

public class Address extends Entity {

    private String street;
    private Integer streetNumber;
    private Integer houseNumber;
    private String phoneNumber;

    @Ignore
    private User owner;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getStreet() {
        return street;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "street='" + street + '\'' +
                ", streetNumber=" + streetNumber +
                ", houseNumber=" + houseNumber +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", owner=" + owner +
                '}';
    }
}

/*
CREATE TABLE `address` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `street` tinytext NOT NULL,
 `houseNumber` int(11) NOT NULL,
 `streetNumber` int(11) DEFAULT NULL,
 `phoneNumber` tinytext,
 PRIMARY KEY (`id`),
 UNIQUE KEY `id` (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 */