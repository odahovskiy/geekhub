package com.geekhub.odahovskiy.lessons._08.lastSeason;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

/**
 * Created by Vova Odahovskiy on 13.11.2014.
 */
public class Main {

    private static DataBase dataBase;

    public static void main(String[] args) {
       start();
    }

    public static void start(){
        doConnection();
        doQueries();
    }

    private static void doQueries() {
        String exitQuery = "exit";
        String query;

        query = inputString("sql>");
        do {
            try {
                String result = dataBase.execute(query);
                System.out.println(result);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            query = inputString("sql>");
        }while (!exitQuery.equals(query.toLowerCase()));
    }

    private static void doConnection()  {

        String[] data;
        boolean done = false;
        do {
            try {
                data = inputConnectData();
                String host = data[0];
                String username = data[1];
                String password = data[2];
                String dbname = data[3];
                dataBase = DataBase.connect(host, username, password, dbname);
                done = true;
                System.out.println("Connection to " + dbname + " success!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!done);
    }
    private static String[] inputConnectData() throws Exception {
        String[] data = new String[4];
        data[0] = inputString("Host: ");
        data[1] = inputString("User: ");
        data[2] = inputString("Password: ");
        data[3] = inputString("db: ");
        return data;
    }


    private static String inputString(String msg)  {
        String inputValue = "Input Error";

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print(msg);
            inputValue = reader.readLine();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        return inputValue;
    }
}
