package com.geekhub.odahovskiy.lessons._08.lastSeason;

import java.sql.*;

/**
 * Created by Vova Odahovskiy on 13.11.2014.
 */
public class DataBase {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql:localhost:3306/";
    private Connection conn = null;
    private static DataBase dataBase;

    private DataBase(String host, String username, String psswd, String dbname) throws Exception {
        String user = username;
        String password = psswd;//Пароль пользователя
        String url = "jdbc:mysql://"+host+":3306/"+dbname;//URL адрес
        String driver = "com.mysql.jdbc.Driver";//Имя драйвера
        try {
            Class.forName(driver);//Регистрируем драйвер
            conn = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (Exception e) {
            throw new Exception(e);
        }

    }

    public synchronized static DataBase connect(String host, String uname, String password, String dbname) throws Exception {
        if (null == dataBase) {
            return new DataBase(host, uname, password, dbname);
        }else
            return dataBase;
    }


    private final static int MIN_QUERY_LENGTH = 8;
    public String execute(String query) throws SQLException {

        if (query.length() < MIN_QUERY_LENGTH) throw new SQLDataException("Bad query");

        String resultMessage = "Bad query, use sql operators like (SELECT,INSERT,UPDATE,DELETE)";
        //take head of query
        String headoperator = query.substring(0, 6).toUpperCase();
       switch (headoperator){
           case "INSERT":
               resultMessage = executeUpdate(query);
                break;
           case "SELECT":
               resultMessage = select(query);
               break;
           case "UPDATE":
               resultMessage = executeUpdate(query);
               break;
           case "DELETE":
               resultMessage = executeUpdate(query);
               break;
       }

        return resultMessage;
    }

    private String executeUpdate(String query) throws SQLException {

        java.sql.Statement st = conn.createStatement();
        int done = st.executeUpdate(query);
        int updateCount = st.getUpdateCount();
        st.close();

        return (done == 1) ?"Query updates= " + updateCount
                           : query + " Failed";

    }


    private String select(String query) throws SQLException {
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int columsCount = rsmd.getColumnCount();

        int size = getResultCount(resultSet);

        String[][] data = new String[size + 1][columsCount];
        fillTitles(rsmd, columsCount, data);
        fillData(query, statement, rsmd, columsCount, data);

        StringBuilder stringBuilder = new StringBuilder();

       String tableView =  makeTable(data);

        return tableView;
    }

    /**
     * Generate graphic view of table
     * @param data - array with data
     */
    private String makeTable(String[][] data) {

        StringBuilder stringBuilder = new StringBuilder();
        for (String[] row : data) {
            for (String col : row) {
                stringBuilder.append(col).append("     ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Fill array by data
     * @param query - query like SELECT * FROM table_name
     * @param statement current statement
     * @param rsmd - result set meta data
     * @param columsCount
     * @param data - array with generated titles
     * @throws java.sql.SQLException
     */
    private void fillData(String query, Statement statement, ResultSetMetaData rsmd, int columsCount, String[][] data) throws SQLException {
        ResultSet resultSet;
        resultSet = statement.executeQuery(query);
        int rowIndex = 1;
        while(resultSet.next()) {
            for (int columnIndex = 1; columnIndex <= columsCount; columnIndex++) {
                String columName = rsmd.getColumnName(columnIndex);
                String value = resultSet.getString(columName);
                data[rowIndex][columnIndex-1] = value;
            }
            rowIndex++;
        }
    }

    /**
     *
     * @param rsmd
     * @param columsCount
     * @param data
     * @throws SQLException
     */
    private void fillTitles(ResultSetMetaData rsmd, int columsCount, String[][] data) throws SQLException {
        for (int columnIndex = 1; columnIndex <= columsCount; columnIndex++) {
            String columName = rsmd.getColumnName(columnIndex);
            data[0][columnIndex-1] = columName+"   ";
        }
    }

    /**
     * return count of result records
     * @param resultSet
     * @return
     * @throws java.sql.SQLException
     */
    private int getResultCount(ResultSet resultSet) throws SQLException {
        int recordsCount =0;
        if (null != resultSet)
        {
            resultSet.beforeFirst();
            resultSet.last();
            recordsCount = resultSet.getRow();
        }

        return recordsCount;
    }


}
