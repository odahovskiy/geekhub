package com.geekhub.odahovskiy.lessons._03.task3;

import com.geekhub.odahovskiy.lessons._03.task3.exceptions.SpeedLimitException;

import java.util.List;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public abstract class Vehicle implements Driveable{

    private int speedLimit;
    private int speed;
    private String model;
    private EnergyProvider energyProvider;
    private List<ForceAcceptor> forceAcceptors;
    private ForceProvider forceProvider;

    public Vehicle() {}

    public Vehicle(String model,int speedLimit){
        this.model = model;
        this.speedLimit = speedLimit;
    }

    public Vehicle(EnergyProvider energyProvider,int speedLimit) {
        this.energyProvider = energyProvider;
        this.speedLimit = speedLimit;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) throws SpeedLimitException {
        if (speed > speedLimit)
            throw new SpeedLimitException("Over speed limit");
        if (speed < 0)
            throw new SpeedLimitException("Nowhere to lower");
        this.speed = speed;
    }

    public void turnOn(){
        forceProvider.turnOn();
    }

    public void turnOff(){
        forceProvider.turnOff();
    }

    public void setForceAcceptor(List<ForceAcceptor> forceAcceptors) {
        this.forceAcceptors = forceAcceptors;
    }

    public void setForceProvider(ForceProvider forceProvider) {
        this.forceProvider = forceProvider;
    }

    public List<ForceAcceptor> getForceAcceptor() {
        return forceAcceptors;
    }

    public String getModel() {
        return model;
    }

    public ForceProvider getForceProvider() {
        return forceProvider;
    }

    public EnergyProvider getEnergyProvider() {
        return energyProvider;
    }

    public void setEnergyProvider(EnergyProvider energyProvider) {
        this.energyProvider = energyProvider;
    }

    protected void setForceSpeed(int speed) {
        if (forceProvider.isWork()){
            for (ForceAcceptor fAcceptor : getForceAcceptor()) {
                fAcceptor.speed(speed);
            }
        }else {
            System.out.println("Turn on engine");
        }
    }

    protected void spinningForce(){
        if (forceProvider.isWork()){
            for (ForceAcceptor fAcceptor : getForceAcceptor()) {
                fAcceptor.spinning();
            }
        }else {
            System.out.println("Turn on engine");
        }
    }

    protected void turnning(){
        for (ForceAcceptor forceAcceptor : getForceAcceptor()) {
            forceAcceptor.turn();
        }
    }

}
