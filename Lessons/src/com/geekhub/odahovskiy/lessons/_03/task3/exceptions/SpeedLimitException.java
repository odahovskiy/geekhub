package com.geekhub.odahovskiy.lessons._03.task3.exceptions;

/**
 * Created by Vova Odahovskiy on 30.10.14.
 */
public class SpeedLimitException extends Exception{
    public SpeedLimitException(String msg){
        super(msg);
    }

    public SpeedLimitException(){
        super("Over Speed limit");
    }
}
