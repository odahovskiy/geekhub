package com.geekhub.odahovskiy.lessons._03.task3;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface ForceProvider {
    void turnOn();
    void turnOff();
    void accelerate();
    void brake();
    boolean isWork();
}
