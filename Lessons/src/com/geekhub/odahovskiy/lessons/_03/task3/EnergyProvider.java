package com.geekhub.odahovskiy.lessons._03.task3;

import com.geekhub.odahovskiy.lessons._03.task3.exceptions.NoFuelException;
import com.geekhub.odahovskiy.lessons._03.task3.exceptions.TankOverFlowException;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface EnergyProvider {
    double takeEnergy(double consumption) throws NoFuelException;
    double getEnergyLvl();
    double fill(double volume) throws TankOverFlowException;
}
