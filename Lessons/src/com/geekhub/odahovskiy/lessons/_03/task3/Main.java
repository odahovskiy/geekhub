package com.geekhub.odahovskiy.lessons._03.task3;

import com.geekhub.odahovskiy.lessons._01.Utill;

/**
 * Created by Вова on 29.10.14.
 */
public class Main {
    public static void main(String[] args) {
        actionMenu(new Car("BMW x5"));
    }

    private static void actionMenu(Driveable driveable){

        int exitCode = 0;
        int command = -1;
        boolean nextSteep = false;

        do {

           command = inputCommand();

           switch (command){
               case 0:
                   System.exit(1);
               case 1:
                   driveable.ride100km();
                   break;
               case 2:
                   driveable.turnOn();
                   break;
               case 3:
                   driveable.fillTank();
                   break;
               case 4:
                   driveable.remainingFuel();
                   break;
               case 5:
                   driveable.improveSpeed();
                   break;
               case 6:
                   driveable.lowerSpeed();
                   break;
               default:continue;
           }
        }while (command != exitCode);
    }

    private static int inputCommand() {

        String command = Utill.getString();

        switch (command){
            case "to": return 2;
            case "g" : return 1;
            case "f" : return 3;
            case "sf": return 4;
            case "us": return 5;
            case "ds": return 6;
            case "exit":return 0;
        }
        if ("?".equals(command)){
            System.out.println("to - turn on car");
            System.out.println("g - ride 100 km");
            System.out.println("f - fill 10 l");
            System.out.println("sf - show fuel");
            System.out.println("us - up speed");
            System.out.println("ds - down speed");
            System.out.println("exit");
        }
        return -1;
    }
}
