package com.geekhub.odahovskiy.lessons._03.task3;

/**
 * Created by Вова on 29.10.14.
 */
public interface Driveable {
    void turnOn();
    void turnOff();
    void accelerate();
    void brake();
    void turn();

    void ride100km();
    void fillTank();
    void improveSpeed();
    void lowerSpeed();
    double remainingFuel();
}
