package com.geekhub.odahovskiy.lessons._03.task3;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Wheel implements ForceAcceptor {

    public enum WHEEL_LOC{FRONT,BACK}

    private WHEEL_LOC location;
    private double speed;

    public Wheel(){
        System.out.println("Wheel activated");
    }

    public Wheel(WHEEL_LOC location){
        this.location = location;
        System.out.println("Wheel activated");
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }



    @Override
    public void turn() {
        if (location == WHEEL_LOC.FRONT)
            System.out.println("Turn front wheel");
    }

    @Override
    public void spinning() {
        if (speed >0)
            System.out.println("Wheel spinning");
    }

    @Override
    public void speed(double value) {
        speed = value;
    }
}
