package com.geekhub.odahovskiy.lessons._03.task3;

/**
 * Created by Вова on 29.10.14.
 */
public class DieselEngine implements ForceProvider {

   private boolean work;

    public DieselEngine() {
        this.work = false;
        System.out.println("Diesel engine activated");
    }

    public boolean isWork(){return work;}
    @Override
    public void turnOn() {
        System.out.println("Diesel engine turned on");
        work = true;
    }

    @Override
    public void turnOff() {
        System.out.println("Diesel engine turned off");
        work = true;
    }

    @Override
    public void accelerate() {
        if (work)
            System.out.println("Ride by diesel engine");
        else
            System.out.println("Turn on engine");
    }

    @Override
    public void brake() {
        System.out.println("Brake");
    }
}
