package com.geekhub.odahovskiy.lessons._03.task3;

import com.geekhub.odahovskiy.lessons._03.task3.exceptions.NoFuelException;
import com.geekhub.odahovskiy.lessons._03.task3.exceptions.TankOverFlowException;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class GasTank implements EnergyProvider {
    private final double MAX_FUEL;
    private double volume;

    public GasTank(double volume,double max_fuel){
        this.volume = volume;
        MAX_FUEL = max_fuel;
        System.out.println("Gas Tank activated, max fuel "+max_fuel);
    }

    @Override
    public double takeEnergy(double consumption) throws NoFuelException {
        if (volume - consumption >=0){
            volume -= consumption;
            return volume;
        }
        throw new NoFuelException();
    }

    @Override
    public double getEnergyLvl() {
        return volume;
    }

    @Override
    public double fill(double vol) throws TankOverFlowException {
        double remainder = (this.volume + vol > MAX_FUEL)? Math.abs(MAX_FUEL - (this.volume+vol)) : 0;
        if (remainder == 0){
            remainder = MAX_FUEL - (volume + vol);
            volume+=vol;
            return remainder;
        }
       throw new TankOverFlowException(remainder);
    }
}
