package com.geekhub.odahovskiy.lessons._03.task3.exceptions;

/**
 * Created by Vova Odahovskiy on 30.10.14.
 */
public class NoFuelException extends Exception {
    public NoFuelException() {
        super("Fuel ended");
    }
}
