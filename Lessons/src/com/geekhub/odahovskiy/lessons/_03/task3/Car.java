package com.geekhub.odahovskiy.lessons._03.task3;

import com.geekhub.odahovskiy.lessons._03.task3.exceptions.NoFuelException;
import com.geekhub.odahovskiy.lessons._03.task3.exceptions.SpeedLimitException;
import com.geekhub.odahovskiy.lessons._03.task3.exceptions.TankOverFlowException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Вова on 29.10.14.
 */
public class Car extends Vehicle {

    private final double FUEL_PER_100_KM = 8;

    public Car(String carModel) {
        super(carModel, 100);//(carName,speed limit)
        setEnergyProvider(new GasTank(25, 50));//(countFuel,maxFuel)
        setForceProvider(new DieselEngine());
        activateWheels();
        System.out.println("Car ["+carModel+"] created");
    }

    private void activateWheels() {
        Wheel front = new Wheel(Wheel.WHEEL_LOC.FRONT);
        Wheel back = new Wheel(Wheel.WHEEL_LOC.BACK);
        List<ForceAcceptor> wheelList = new ArrayList<>(Arrays.asList(front, front, back, back));
        setForceAcceptor(wheelList);
    }

    @Override
    public void accelerate() {
        if (getForceProvider().isWork()){
            try {
                getForceProvider().accelerate();
                setForceSpeed(getSpeed());
                System.out.println("In tank "+getEnergyProvider().takeEnergy(FUEL_PER_100_KM)+"l.");
                spinningForce();
            } catch (NoFuelException e) {
                System.out.println(e.getMessage());
            }
        }else {
            System.out.println("Turn on engine");
        }
    }

    @Override
    public void brake() {
        try {
            getForceProvider().brake();
            setForceSpeed(0);
            setSpeed(0);
        } catch (SpeedLimitException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void turn() {
       turnning();
    }

    @Override
    public void ride100km() {
        if (getForceProvider().isWork()) {
            try {
                getEnergyProvider().takeEnergy(FUEL_PER_100_KM);
            } catch (NoFuelException e) {
                System.err.println(e.getMessage());
            }
        }else {
            System.out.println("Turn on car");
        }
    }

    @Override
    public void fillTank() {
        try {
            getEnergyProvider().fill(10);
        } catch (TankOverFlowException tankOverFlowException) {
            System.err.println(tankOverFlowException.getMessage());
        }
    }

    @Override
    public void improveSpeed() {
        try {
            int curr_speed = getSpeed();
            int nextDivision = curr_speed+10;
            setSpeed(nextDivision);
        } catch (SpeedLimitException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void lowerSpeed() {
        try {
            int curr_speed = getSpeed();
            int nextDivision = curr_speed-10;
            setSpeed(nextDivision);
        } catch (SpeedLimitException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public double remainingFuel() {
        System.out.printf("%nRemaining fuel %f%n", getEnergyProvider().getEnergyLvl());
        return getEnergyProvider().getEnergyLvl();
    }


}
