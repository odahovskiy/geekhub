package com.geekhub.odahovskiy.lessons._03.task3.exceptions;

/**
 * Created by Vova Odahovskiy on 30.10.14.
 */
public class TankOverFlowException extends Exception {
    public TankOverFlowException() {
        super("Tank overflow");
    }
    public TankOverFlowException(double remaining) {
        super("Tank overflow more than on " + remaining + "l");
    }
}
