package com.geekhub.odahovskiy.lessons._03.task2;
import static java.lang.System.out;
/**
 * Created by Vova Odahovskiy on 30.10.14.
 */
public class Main {

    public static void main(String[] args) {
        out.printf("String %f", testString());
        out.printf("String Builder %f", testStringBuilder());
        out.printf("String Buffer %f", testStringBuff());
    }

    public static long testString(){

        String concats = "";

        long startTime = System.currentTimeMillis();
        for (int i =0; i < 50000; i++) {
            concats += String.valueOf(i);
        }
        long endTime = System.currentTimeMillis();

        return endTime-startTime;
    }

    public static long testStringBuilder(){

        StringBuilder sb = new StringBuilder();

        long startTime = System.currentTimeMillis();
        for (int i =0; i < 50000; i++) {
            sb.append(String.valueOf(i));
        }
        long endTime = System.currentTimeMillis();

        return endTime-startTime;
    }

    public static long testStringBuff() {

        StringBuffer sb = new StringBuffer();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 50000; i++) {
            sb.append(String.valueOf(i));
        }
        long endTime = System.currentTimeMillis();

        return endTime - startTime;
    }
}
