package com.geekhub.odahovskiy.lessons._03.task1;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Main {
    public static void main(String[] args) {
        Order o1 = new Order("Черепашки нінзя (12шт)", 315);
        Order o2 = new Order("Люди х 3 (1 шт)", 35);
        Order o3 = new Order("Мачо і ботан (5 шт)", 275);
        Order o4 = new Order("Колобок (2 шт)", 35);
        Order o5 = new Order("iPhone 5s", 7850);
        Order o6 = new Order("Simens m65", 25);

        Order[] orders = {o1, o2, o3, o4, o5, o6};

        print(orders);//print first state
        print(Sorter.sort(orders));//after sort
        print(Sorter.sort2(orders));//after sort
    }

    public static void print(Comparable[] comparables){
        System.out.println("--------");
        for (Comparable order : comparables)
            System.out.println(order);
        System.out.println("--------");
    }
}
