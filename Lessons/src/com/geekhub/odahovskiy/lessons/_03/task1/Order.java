package com.geekhub.odahovskiy.lessons._03.task1;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Order implements Comparable{

    private String description;
    private double totalSumm;

    public Order(String description, double totalSumm) {
        this.description = description;
        this.totalSumm = totalSumm;
    }

    public String getDescription() {
        return description;
    }

    public double getTotalSumm() {
        return totalSumm;
    }

    @Override
    public int compareTo(Object o) {
        if (null != o)// if object not null
            if (o instanceof Order){ //and if object is Order
                if (this.totalSumm < ((Order) o).getTotalSumm()) return -1;
                if (this.totalSumm > ((Order) o).getTotalSumm())  return 1;
                return 0;
            }
        return -2;
    }

    @Override
    public String toString() {
        return description+" "+totalSumm;
    }
}
