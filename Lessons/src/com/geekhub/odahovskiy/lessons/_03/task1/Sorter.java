package com.geekhub.odahovskiy.lessons._03.task1;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Sorter {
    public static Comparable[] sort(Comparable[] comparables){
        Comparable[] tmp = Arrays.copyOfRange(comparables,0,comparables.length);//copy array to var tmp
        Arrays.sort(tmp);//sort copy of array
        return tmp;//returm sorted copy of array
    }
    public static Comparable[] sort2(Comparable[] comparables){
        Comparable[] tmp = Arrays.copyOfRange(comparables,0,comparables.length);
        Arrays.sort(tmp, (a,b) -> b.compareTo(a));
        return tmp;
    }
}
