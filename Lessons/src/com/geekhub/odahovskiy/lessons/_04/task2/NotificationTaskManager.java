package com.geekhub.odahovskiy.lessons._04.task2;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Vova Odahovskiy on 04.11.14..
 */
public class NotificationTaskManager implements TaskManager {

    private TaskManager taskManager;

    public NotificationTaskManager(TaskManager taskManager){
        this.taskManager = taskManager;
    }

    @Override
    public void addTask(Date date, Task task) {
        System.out.printf("Adding %s for %s\n", task, getFormatedDate(date));
        taskManager.addTask(date, task);
    }

    @Override
    public void removeTask(Date date) throws Exception {
        System.out.printf("Removing tasks for %s\n",getFormatedDate(date));
        taskManager.removeTask(date);
    }

    @Override
    public Collection<String> getCategories() {
        System.out.printf("Getting categories\n");
        return taskManager.getCategories();
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        System.out.printf("Getting tasks by categories\n");
        return taskManager.getTasksByCategories();
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        System.out.printf("Getting tasks by category %s\n",category);
        return taskManager.getTasksByCategory(category);
    }

    @Override
    public List<Task> getTasksForToday() {
        System.out.printf("Getting tasks for today\n");
        return taskManager.getTasksForToday();
    }

    public static String getFormatedDate(Date date){
        SimpleDateFormat dateFormater = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        return dateFormater.format(date.getTime());
    }
}
