package com.geekhub.odahovskiy.lessons._04.task2;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Vova Odahovskiy on 04.11.14..
 */
public class PowerManager implements TaskManager {

    private Multimap<Date, Task> tasks;

    public PowerManager(){
        tasks = ArrayListMultimap.create();
    }

    @Override

    public void addTask(Date date, Task task) {
        tasks.put(date, task);
    }

    @Override
    public void removeTask(Date date) throws Exception {
        tasks.removeAll(date);
    }

    @Override
    public Collection<String> getCategories() {
        return tasks.values().stream().map(Task::getCategory).collect(Collectors.toList());
    }

    /*Later remake to guava map*/
    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> resultMap = new HashMap<>();

        for (Task task : tasks.values()) {
            List<Task> taskList = resultMap.containsKey(task.getCategory()) ? resultMap.get(task.getCategory())
                                                                            : new ArrayList<>(Arrays.asList(task));
            taskList.add(task);
            resultMap.put(task.getCategory(),taskList);
        }

        return resultMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        return getTasksByCategories().get(category);
    }

    @Override
    public List<Task> getTasksForToday() {

        Date today = new Date();
        List<Task> taskList = new ArrayList<>();
        tasks.keys().stream().filter(day -> isSame(today, day)).forEach(days -> taskList.addAll(tasks.get(days)));

        return taskList;
    }

    private boolean isSame(Date first, Date second) {

        boolean day = first.getDay() == second.getDay();
        boolean year = first.getYear() == second.getYear();
        boolean month = first.getMonth() == second.getMonth();

        return (day == year == month) ? true : false;
    }
}
