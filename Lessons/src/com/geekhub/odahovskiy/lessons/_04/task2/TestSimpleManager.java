package com.geekhub.odahovskiy.lessons._04.task2;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Vova Odahovskiy on 03.11.14..
 */
public class TestSimpleManager {
    public static void main(String[] args) {
        TaskManager taskManager = new SimpleManager();

        taskManager.addTask(new Date(), new Task("Cat1", "Some task 1"));
        taskManager.addTask(new Date(1000), new Task("Cat2", "Some task 2"));
        taskManager.addTask(new Date(2000000), new Task("Cat3", "Some task 3"));
        taskManager.addTask(new Date(32500000), new Task("Cat4", "Some task 4"));
        taskManager.addTask(new Date(123456789), new Task("Cat5", "Some task 5"));
        taskManager.addTask(new Date(500000000), new Task("Cat1", "Some task 6"));
        taskManager.addTask(new Date(320000), new Task("Cat2", "Some task 7"));
        taskManager.addTask(new Date(90000000), new Task("Cat1", "Some task 8"));

        System.out.printf("Categories %s\n\n", taskManager.getCategories());
        for (Map.Entry<String, List<Task>> list : taskManager.getTasksByCategories().entrySet()) {
            System.out.printf("Category %s\n", list.getKey());
            for (Task task : list.getValue()) {
                System.out.printf("%s\n", task);
            }
        }
        System.out.printf("Cat1 task %s\n\n", taskManager.getTasksByCategory("Cat1"));
        System.out.printf("Task for day %s\n", taskManager.getTasksForToday());
    }
}
