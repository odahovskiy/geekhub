package com.geekhub.odahovskiy.lessons._04.task2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Vova Odahovskiy on 03.11.14.
 */
public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date) throws Exception;
    public Collection<String> getCategories();
    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();
}
