package com.geekhub.odahovskiy.lessons._04.task2;

/**
 * Created by Vova Odahovskiy on 03.11.14.
 */
public class Task {
    private String category;
    private String description;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "Cat:'" + category + '\'' +
                ", Desc:'" + description + '\'' +
                '}';
    }
}
