package com.geekhub.odahovskiy.lessons._04.task2;

import com.geekhub.odahovskiy.lessons._01.Utill;

import java.io.*;
import java.util.*;

/**
 * Created by Vova Odahovskiy on 04.11.14..
 */
public class TestPowerManager {

    public static void main(String[] args) {
        TaskManager powerManager = new NotificationTaskManager(new PowerManager());
        menu(powerManager);
    }

    private static void menu(TaskManager powerManager) {

        boolean work = true;
        int inputOption = -1;
        do {
            System.out.printf("1.Add task\n");
            System.out.printf("2.Remove task\n");
            System.out.printf("3.Get categories\n");
            System.out.printf("4.Get tasks by categories\n");
            System.out.printf("5.Get tasks by category\n");
            System.out.printf("6.Get tasks for today\n");
            System.out.printf("0.Exit\n");
            inputOption = Integer.valueOf(getString("Chose: "));
            
            switch (inputOption){
                //exit
                case 0:{System.exit(1);}
                //add task
                case 1: {
                    addTask(powerManager);
                    break;
                }
                case 2: {
                    removeTask(powerManager);
                    break;
                }
                case 3: {
                    showCategories(powerManager);
                    break;
                }
                case 4:{
                    showTasksByCategories(powerManager);
                    break;
                }
                case 5:{
                    showTaskByCategory(powerManager);
                    break;
                }
                case 6:{
                    showTasksForToday(powerManager);
                    break;
                }
                default:continue;
            }
            
        } while (work);
    }

    private static void showTasksForToday(TaskManager powerManager) {
        System.out.printf("Tasks for today: ");
        List<Task> tasks = powerManager.getTasksForToday();
        for (Task task : tasks) {
            System.out.printf("%s", task);
        }
    }

    private static void showTaskByCategory(TaskManager powerManager) {
        System.out.printf("Input category name: ");
        String category = Utill.getString();
        List<Task> tasks = powerManager.getTasksByCategory(category);
        for (Task task : tasks) {
                System.out.printf("%s", task);
        }
    }

    private static void showTasksByCategories(TaskManager powerManager) {
        Map<String, List<Task>> list = powerManager.getTasksByCategories();
        for (Map.Entry<String, List<Task>> category : list.entrySet()) {
            System.out.printf("Category %s", category);
            for (Task task : category.getValue()) {
                System.out.printf("%s", task);
            }
        }
    }

    private static void showCategories(TaskManager powerManager) {
        System.out.printf("%s\n", powerManager.getCategories());
    }

    private static void addTask(TaskManager powerManager) {
        Date date = inputDate();
        Task task = makeTask();
        powerManager.addTask(date,task);
    }

    private static void removeTask(TaskManager powerManager) {
        try {
            Date date = inputDate();
            powerManager.removeTask(date);
        } catch (Exception e) {
            System.out.printf("%s\n", e.getMessage());
        }

    }

    private static Task makeTask() {
        boolean nextStep = false;
        String category = null;
        String description = null;
        category = getString("\nInput category name:");
        description = getString("\nInput task description:");

        return new Task(category, description);
    }

    //
    private static String getString(String msg) {

        boolean nextStep;
        String description = "";
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        try {
                System.out.printf(msg);
                description = br.readLine().trim();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return description;
    }


    private static Date inputDate() {

        boolean nextStep = false;
        Date date = null;
        long millis;
        do{
            try {
                millis = Long.valueOf(getString("Input date in millis:"));
                date = new Date(millis);
                System.out.printf("Date is %s\n", NotificationTaskManager.getFormatedDate(date));
                nextStep = true;
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }while (!nextStep);

        return date;
    }
    
}
