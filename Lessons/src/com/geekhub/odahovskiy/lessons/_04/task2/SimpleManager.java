package com.geekhub.odahovskiy.lessons._04.task2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Vova Odahovskiy on 03.11.14..
 */
public class SimpleManager implements TaskManager {
    private Map<Date, Task> tasks;

    public SimpleManager(){
        tasks = new HashMap<>();
    }

    @Override
    public void addTask(Date date, Task task) {
        tasks.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        if (null == tasks.get(date)) throw new NullPointerException("Date should be not null!");
            tasks.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<>(Collections.EMPTY_SET);
        categories.addAll(tasks.entrySet().stream().map(task -> task.getValue().getCategory()).collect(Collectors.toList()));
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {

        Map<String, List<Task>> resultMap = new TreeMap<>(Collections.EMPTY_MAP);

        for (Map.Entry<Date, Task> day : tasks.entrySet()) {
            Task task = day.getValue();//get currentSeason task
            String category = task.getCategory();//get task category
            //get task list for this category
            List<Task> resultTaskList = (null == resultMap.get(category)) ? new ArrayList<>() : resultMap.get(category);
            resultTaskList.add(task);
            resultMap.put(category, resultTaskList);
        }

        return resultMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {

        List<Task> resultList = new ArrayList<>(Collections.EMPTY_LIST);

        for (Map.Entry<Date, Task> day : tasks.entrySet()) {
            Task task = day.getValue();//get currentSeason task
            if (category.equals(task.getCategory())) resultList.add(task);
        }

        return resultList;
    }

    @Override
    public List<Task> getTasksForToday() {

        Date today = new Date();
        List<Task> taskList = tasks.entrySet().stream()
                                    .filter(days -> isSame(today, days.getKey()))
                                    .map(Map.Entry::getValue).collect(Collectors.toList());

        return taskList;
    }
    private boolean isSame(Date first, Date second) {

        boolean day = first.getDay() == second.getDay();
        boolean year = first.getYear() == second.getYear();
        boolean month = first.getMonth() == second.getMonth();

        return  (day == year == month)? true:false;
    }
}
