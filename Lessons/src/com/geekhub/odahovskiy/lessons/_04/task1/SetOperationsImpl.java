package com.geekhub.odahovskiy.lessons._04.task1;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vova Odahovskiy on 03.11.14.
 */
public class SetOperationsImpl implements SetOperations {
    @Override
    public boolean equals(Set A, Set B) {
        return A.equals(B);
    }

    @Override
    public Set union(Set A, Set B) {
        Set C = new HashSet<>(A);
        C.addAll(B);
        return C;
    }

    @Override
    public Set subtract(Set A, Set B) {
        Set C = new HashSet<>(A);
        C.removeAll(B);
        return C;
    }

    @Override
    public Set intersect(Set A, Set B) {
        Set C = new HashSet<>(A);
        C.retainAll(B);
        return C;
    }

    @Override
    public Set symmetricSubtract(Set A, Set B) {
        return union(subtract(A, B), subtract(B,A));
    }
}
