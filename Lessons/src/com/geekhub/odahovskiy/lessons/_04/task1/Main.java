package com.geekhub.odahovskiy.lessons._04.task1;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vova Odahovskiy on 03.11.14.
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> a = new HashSet<Integer>(Collections.EMPTY_SET);
        Set<Integer> b = new HashSet<Integer>(Collections.EMPTY_SET);
        Set<Integer> resultSet = new HashSet<Integer>(Collections.EMPTY_SET);

        for (int i = 0; i< 10; i++) {
            a.add(i);
            b.add(i + 2);
        }

        SetOperations operations = new SetOperationsImpl();

        System.out.printf("SET A = %s\n", a);
        System.out.printf("SET B = %s\n", b);
        System.out.printf("EQUALS A && B = %s\n", operations.equals(a,b));

        resultSet.addAll(operations.union(a, b));
        System.out.printf("UNION A B = %s\n", resultSet);
        resultSet.clear();

        resultSet.addAll(operations.subtract(a, b));
        System.out.printf("SUBSTRACT A B = %s\n", resultSet);
        resultSet.clear();

        resultSet.addAll(operations.intersect(a, b));
        System.out.printf("INTERSECT A B = %s\n", resultSet);
        resultSet.clear();

        resultSet.addAll(operations.symmetricSubtract(a, b));
        System.out.printf("Symmetric Subtract A B = %s\n", resultSet);
        resultSet.clear();

    }

}
