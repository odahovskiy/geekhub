package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface ForceProvider {
    void turnOn();
    void turnOff();
    void accelerate();
    void brake();
    boolean isWork();
}
