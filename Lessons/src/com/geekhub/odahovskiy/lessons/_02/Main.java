package com.geekhub.odahovskiy.lessons._02;

import com.geekhub.odahovskiy.lessons._01.Utill;


/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Main {
    public static void main(String[] args) {
        menu();
    }

    private static void menu(){
        int exit_code = 0;
        int val = -1;
        boolean nextSteep = false;

        do {

            do {
                System.out.println("Choose type of Vehicle");
                System.out.println("1.Car | 2.SolarPoweredCar | 3.Boat");
                System.out.println("0 exit");
                val = Utill.getInteger("Input value");
                switch (val){
                    case 0:
                        System.exit(1);
                    case 1: {
                        actionMenu(new Car("Car", 50));
                        break;
                    }
                    case 2: {
                        actionMenu(new SolarPoweredCar("SolarPoweredCar", 60));
                        break;
                    }

                    case 3: {
                        actionMenu(new Boat("Boat", 400));
                        break;
                    }
                    default:continue;
                }
            } while (!nextSteep);


        }while (val != exit_code);
    }

    private static void actionMenu(Driveable driveable){
        int exit_code = 0;
        int val = -1;
        boolean nextSteep = false;

        do {

            do {
                System.out.println("Choose action");
                System.out.println("1.turn On | 2.turnOff | 3.Accelerate | 4.brake | 5.turn");
                System.out.println("0 exit");
                val = Utill.getInteger("Input value");
                switch (val){
                    case 0:
                        System.exit(1);
                    case 1:{
                        driveable.turnOn();
                        break;
                    }
                    case 2:{
                        driveable.turnOff();
                        break;
                    }

                    case 3:{
                        driveable.accelerate();
                        break;
                    }
                    case 4:{
                        driveable.brake();
                        break;
                    }
                    case 5:{
                        driveable.turn();
                        break;
                    }
                    default:continue;
                }
            } while (!nextSteep);


        }while (val != exit_code);
    }
}
