package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface Driveable {
    void turnOn();
    void turnOff();
    void accelerate();
    void brake();
    void turn();
}
