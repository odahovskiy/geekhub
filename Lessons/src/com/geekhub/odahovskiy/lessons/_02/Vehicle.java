package com.geekhub.odahovskiy.lessons._02;

import java.util.List;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public abstract class Vehicle implements Driveable{

    private String model;
    private EnergyProvider energyProvider;
    private List<ForceAcceptor> forceAcceptors;
    private ForceProvider forceProvider;

    public Vehicle() {

    }

    public Vehicle(String model){
        this.model = model;
    }
    public Vehicle(EnergyProvider energyProvider) {
        this.energyProvider = energyProvider;

    }

    public void turnOn(){
        forceProvider.turnOn();
    }
    public void turnOff(){
        forceProvider.turnOff();
    }
    public void setForceAcceptor(List<ForceAcceptor> forceAcceptors) {
        this.forceAcceptors = forceAcceptors;
    }

    public void setForceProvider(ForceProvider forceProvider) {
        this.forceProvider = forceProvider;
    }

    public List<ForceAcceptor> getForceAcceptor() {
        return forceAcceptors;
    }

    public String getModel() {
        return model;
    }


    public ForceProvider getForceProvider() {
        return forceProvider;
    }

    public EnergyProvider getEnergyProvider() {
        return energyProvider;
    }

    public void setEnergyProvider(EnergyProvider energyProvider) {
        this.energyProvider = energyProvider;
    }

    protected void setForceSpeed(int speed) {
        if (forceProvider.isWork()){
            for (ForceAcceptor fAcceptor : getForceAcceptor()) {
                fAcceptor.speed(speed);
            }
        }else {
            System.out.println("Turn on engine");
        }
    }

    protected void spinningForce(){
        if (forceProvider.isWork()){
            for (ForceAcceptor fAcceptor : getForceAcceptor()) {
                fAcceptor.spinning();
            }
        }else {
            System.out.println("Turn on engine");
        }
    }

    protected void turnning(){
        for (ForceAcceptor forceAcceptor : getForceAcceptor()) {
            forceAcceptor.turn();
        }
    }

}
