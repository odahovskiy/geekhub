package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class SolarBattery implements EnergyProvider {
    private double volume;
    public SolarBattery(double volume){
        this.volume = volume;
        System.out.println("Solar battery activated");
    }
    @Override
    public double takeEnergy(double consumption) {
        if (volume - consumption >=0){
            volume -= consumption;
            return volume;
        }
        System.out.println("Battery ended");
        return volume;
    }

    @Override
    public double getEnergyLvl() {
        return volume;
    }
}
