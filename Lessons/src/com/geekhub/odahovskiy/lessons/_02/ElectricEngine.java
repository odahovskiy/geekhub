package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class ElectricEngine implements ForceProvider {
    private boolean work;

    public ElectricEngine(){
        work = false;
        System.out.println("Electronic engine activated");
    }
    @Override
    public void turnOn() {
        System.out.println("Electronic engine turned on");
        work = true;
    }

    @Override
    public void turnOff() {
        System.out.println("Electronic engine turned on");
        work = true;
    }

    @Override
    public void accelerate() {
        if(work)
            System.out.println("Ride by electronic engine");
        else
            System.out.println("Turn on engine");
    }

    @Override
    public void brake() {
        System.out.println("brake");
    }

    @Override
    public boolean isWork() {
        return work;
    }
}
