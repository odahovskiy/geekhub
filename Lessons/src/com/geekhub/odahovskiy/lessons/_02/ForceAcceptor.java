package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface ForceAcceptor {
    void turn( );
    void spinning( );
    void speed(double value);
}
