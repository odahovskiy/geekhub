package com.geekhub.odahovskiy.lessons._02;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Boat extends Vehicle {
    public Boat(String nameModel,double fuel) {
        super(nameModel);
        setEnergyProvider(new GasTank(fuel));
        setForceAcceptor(new ArrayList<ForceAcceptor>(Arrays.asList(new Propeller())));
        setForceProvider(new DieselEngine());
        System.out.println("Boat ["+nameModel+"] created");
    }


    @Override
    public void accelerate() {
        if (getForceProvider().isWork()){
            getForceProvider().accelerate();
            setForceSpeed(60);
            System.out.println("In tank " + getEnergyProvider().takeEnergy(12.5) + "l.");
        }else {
            System.out.println("Turn on engine");
        }
    }

    @Override
    public void brake() {
        getForceProvider().brake();
        setForceSpeed(0);
    }

    @Override
    public void turn() {
        turnning();
    }
}
