package com.geekhub.odahovskiy.lessons._02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Car extends Vehicle {

    public Car(String carModel,double fuel) {
        super(carModel);
        setEnergyProvider(new GasTank(fuel));
        setForceProvider(new DieselEngine());
        activateWheels();
        System.out.println("Car ["+carModel+"] created");
    }

    private void activateWheels() {
        Wheel front = new Wheel(Wheel.WHEEL_LOC.FRONT);
        Wheel back = new Wheel(Wheel.WHEEL_LOC.BACK);
        List<ForceAcceptor> wheelList = new ArrayList<>(Arrays.asList(front, front, back, back));
        setForceAcceptor(wheelList);
    }

    @Override
    public void accelerate() {
        if (getForceProvider().isWork()){
            getForceProvider().accelerate();
            setForceSpeed(60);
            System.out.println("In tank "+getEnergyProvider().takeEnergy(3.9)+"л.");
            spinningForce();
        }else {
            System.out.println("Turn on engine");
        }
    }

    @Override
    public void brake() {
        getForceProvider().brake();
        setForceSpeed(0);
    }

    @Override
    public void turn() {
       turnning();
    }



}
