package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public interface EnergyProvider {
    double takeEnergy(double consumption);
    double getEnergyLvl();
}
