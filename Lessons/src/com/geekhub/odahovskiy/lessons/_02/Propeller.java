package com.geekhub.odahovskiy.lessons._02;

/**
 * Created by Vova Odahovskiy on 29.10.14.
 */
public class Propeller implements ForceAcceptor {
    private double speed;

    public Propeller(){
        System.out.println("Propeller activated");
    }

    @Override
    public void turn() {
        System.out.println("Turn propeller");
    }

    @Override
    public void spinning() {
        if (speed >0)
            System.out.println("Propeller spinning");
    }

    @Override
    public void speed(double value) {
        speed = value;
    }
}
