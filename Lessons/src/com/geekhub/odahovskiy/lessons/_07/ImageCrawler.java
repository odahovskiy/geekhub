package com.geekhub.odahovskiy.lessons._07;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;

    //list of image extensions
    private final String[] imageExtensions = new String[]{"BMP","CPT","GIF","HDR","JPEG","PCX","PDN","PNG","PSD",
            "RAW","TGA","TIFF","WDP","HDP","XPM","PDF","SVG","GIF"};

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
        createDirIfNotExist(folder);
    }

    private void createDirIfNotExist(String path) {
        Path dir = new File(path).toPath();
        if(Files.notExists(dir)){
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Call this method to start download images from specified URL.
     * @param urlToPage
     * @throws java.io.IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        Collection<URL> images = page.getImageLinks().stream().distinct().collect(Collectors.toSet());

        images.stream().filter(image -> isImageURL(image)).forEach(image -> {
            executorService.execute(new ImageTask(image, folder));
        });
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        for (String extension : imageExtensions) {
            if (url.toString().toUpperCase().contains(extension)) {
                return true;
            }
        }
        return false;
    }



}
