package com.geekhub.odahovskiy.lessons._01.task2;

/**
 * Created by Вова on 15.10.14.
 */
public class Fibonachi {
    public static int getFibonachi(int indx) {
        return  fibonachi(indx);
    }

    private static int fibonachi ( int indx ) {
        return indx < 0 ?
                ( fibonachi( indx + 2 ) - fibonachi( indx + 1 ) ) :
                indx == 0 ?
                        0 :
                        indx <= 2 ?
                                1 :
                                ( fibonachi( indx - 2 ) + fibonachi( indx - 1 ) );
    }
}
