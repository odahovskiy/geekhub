package com.geekhub.odahovskiy.lessons._01.task1;

import com.geekhub.odahovskiy.lessons._01.Utill;

/**
 * Created by Вова on 17.10.14.
 */
public class Main {
    public static void main(String[] args) {
        int val = Utill.getInteger("Write number");
        int ans = Factorial.getFactorial(val);
        System.out.println(ans);
    }
}
