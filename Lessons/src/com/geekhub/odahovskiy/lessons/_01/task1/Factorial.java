package com.geekhub.odahovskiy.lessons._01.task1;

/**
 * Created by Вова on 17.10.14.
 */
public class Factorial {
    public static int getFactorial(int value) {
        return factorial(value);
    }

    private static int factorial(int value){
        return (0 == value)? 1: value * factorial(value - 1);
    }
}
