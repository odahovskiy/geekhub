package com.geekhub.odahovskiy.lessons._01.task3;

import com.geekhub.odahovskiy.lessons._01.Utill;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Вова on 17.10.14.
 */
public class Main {
    public static void main(String[] args) {
        int digit = getValue();
        Converter.ConvertWay type = Converter.getConvertType();
        System.out.println(Converter.Convert(digit, type));
    }

    private static int getValue(){
        boolean ok = true;
        Pattern pattern = Pattern.compile("[0-9]");
        int value = 0;//contains input value
        do {
            value = Utill.getInteger("Write digit from 0 - 9");
            Matcher matcher = pattern.matcher(String.valueOf(value));
            //if all good and digit between 0 - 9 return value else try input
            if (matcher.matches()) return value;
            System.err.println("Not correct!");
            ok = false;
        }while (!ok);
       return value;
    }
}
