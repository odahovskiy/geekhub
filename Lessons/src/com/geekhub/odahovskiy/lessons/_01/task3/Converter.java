package com.geekhub.odahovskiy.lessons._01.task3;

import java.util.Scanner;

/**
 * Created by Вова on 17.10.14.
 */
public class Converter {
    private static final String[] digits = {
            "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь","девять"};

    public enum ConvertWay {CASE,IF_ELSE,DIRECT_ACCESS}

    public static String Convert(Integer value) {
        return digits[value];
    }

    public static String Convert(Integer value, ConvertWay type) {
        switch (type) {
            //case type
            case CASE: {
                switch (value) {
                    case 0:
                        return digits[0];
                    case 1:
                        return digits[1];
                    case 2:
                        return digits[2];
                    case 3:
                        return digits[3];
                    case 4:
                        return digits[4];
                    case 5:
                        return digits[5];
                    case 6:
                        return digits[6];
                    case 7:
                        return digits[7];
                    case 8:
                        return digits[8];
                    case 9:
                        return digits[9];
                }
            }
            //IF ELSE
            case IF_ELSE:{
                if (value == 0){
                    return digits[0];
                }else if (value == 1){
                    return digits[1];
                }else if (value == 2){
                    return digits[2];
                }else if (value == 3){
                    return digits[3];
                }else if (value == 4){
                    return digits[4];
                }else if (value == 5){
                    return digits[5];
                }else if (value == 6){
                    return digits[6];
                }else if (value == 7){
                    return digits[7];
                }else if (value == 8){
                    return digits[8];
                }else {
                    return digits[9];
                }
            }
            //DIRECT ACCESS
            case DIRECT_ACCESS: {
                return digits[value];
            }
        }
        return "";
    }
    public static ConvertWay getConvertType(){
        boolean ok = true;
        int type = 3;
        do {
            Scanner in = new Scanner(System.in);
            System.out.println("Choose convert type 1 Case, 2 If Else, 3 Direct Access");
            type = in.nextInt();
            switch (type) {
                case 1: {
                    return Converter.ConvertWay.CASE;
                }
                case 2: {
                    return Converter.ConvertWay.IF_ELSE;
                }
                case 3: {
                    return Converter.ConvertWay.DIRECT_ACCESS;
                }
                default: {
                    System.err.println("Wrong value");
                    ok = false;
                }
            }
        }while (!ok);
        return Converter.ConvertWay.DIRECT_ACCESS;
    }

}
