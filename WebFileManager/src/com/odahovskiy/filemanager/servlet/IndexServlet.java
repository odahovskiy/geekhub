package com.odahovskiy.filemanager.servlet;

import com.odahovskiy.filemanager.util.FileManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Vova Odahovskiy on 05.12.2014.
 */
@WebServlet(name = "indexServlet",urlPatterns = "/index")
public class IndexServlet extends HttpServlet {

    private FileManager fileManager;

    @Override
    public void init() throws ServletException {
        super.init();
        String mainDir =  getServletContext().getRealPath("/upload");
        fileManager = new FileManager(mainDir);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String,String> fileList = null;
        HttpSession session = req.getSession();
        String action = req.getParameter("action");
        String url = req.getParameter("url");

        if (null == url) {
            fileList = fileManager.getFileList(null);
            session.setAttribute("back_link","/index");
        }else {
            switch (action) {
                case "open":
                    fileList = fileManager.getFileList(url);
                    int index = url.lastIndexOf("\\");
                    String backUrl = (index != -1) ? "/index?url=" + url.substring(0,index)+"&action=open"
                                                   : "/index";
                    session.setAttribute("back_link",backUrl);
                    List<String> lst = fileList.keySet().stream().filter(str -> str.contains("file:")).collect(Collectors.toList());
                    if (lst.size() > 0) {
                        String fileName = String.valueOf(lst.toArray()[0]);
                        session.setAttribute("fileName", fileName);
                        session.setAttribute("file", fileList.get(fileName));
                        if (fileName.length() > 5) {
                            req.getRequestDispatcher("/showFile.jsp").forward(req, resp);

                        }else {
                            //if error with read. Know that can do more beauty, but..
                            req.getRequestDispatcher("/showFileError.jsp").forward(req, resp);
                        }
                        return;
                    }break;
                case "delete":
                    fileManager.deleteFile(url);
                    resp.sendRedirect("/index");
                    return;
            }
        }

            session.setAttribute("fileList", fileList);
            req.getRequestDispatcher("/main.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        String content = req.getParameter("area");
        String path = req.getParameter("url");
        if ("update".equals(action)) {
            path = path.substring(path.indexOf("file:") + 5, path.length());
            fileManager.updateFile(path, content);
        }
        int index = path.lastIndexOf("\\");
        String backUrl = (index != -1) ? "/index?url=" + path.substring(0,index)+"&action=open"
                : "/index";
        resp.sendRedirect(backUrl);
    }
}
