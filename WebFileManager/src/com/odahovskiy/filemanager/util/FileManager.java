package com.odahovskiy.filemanager.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Vova Odahovskiy on 05.12.2014.
 */
public class FileManager {
    /* basic storage folder*/
    private final String mainDir;

    public FileManager(String mainDir){
        this.mainDir = mainDir;
    }

    public Map<String,String> getFileList(String path) {
        Map<String, String> fileList = Collections.EMPTY_MAP
        File dir;
        /* Moment  when we are  opening not main dir */
        if (null != path) {
            String fullPath = mainDir + "\\" + path; // getting full path to dir of file
            dir = new File(fullPath);
            /* if is directory , return file list back*/
            if (dir.isDirectory()) {
              for (File file : dir.listFiles()){
                  fileList.put(file.getName(), file.getPath().substring(file.getPath().indexOf("\\upload\\") + 8, file.getPath().length()));
              }
            }else {
                /* read file and return file content to show in web page*/
                try {
                    String content = Files.readAllLines(dir.toPath()).stream().collect(Collectors.joining("\n"));
                    fileList.put("file:"+dir.getPath().substring(dir.getPath().indexOf("\\upload\\") + 8, dir.getPath().length()), content);
                } catch (IOException e) {
                    fileList.put("file:", "Problem with read file " + dir.getName()+"</br>"+e.getMessage());
                }
            }
        }else {
            dir = new File(mainDir);
            if (dir.isDirectory()) {
                for (File file : dir.listFiles()){
                    String key = file.getName();
                    String value = file.getPath().substring(file.getPath().indexOf("\\upload\\") + 8, file.getPath().length());
                    fileList.put(key,value);
                }
            }
        }
        return fileList;
    }

    public boolean deleteFile(String filePath) {
        filePath = mainDir + "\\" + filePath;
        File file = new File(filePath);
        if (file.isFile()){
           if(file.exists()) {
               System.out.println(filePath);
              return file.delete();
           }else return false;
        }else {
            return removeDirectory(file);
        }
    }

    public void updateFile(String filePath,String content) {
        String fullPath = mainDir + "\\" + filePath;
        File file = new File(fullPath);
        if (file.exists()){
            if (file.isFile()) {
                file.delete();
            }
            try  {
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();
            }catch (IOException ex){
                ex.printStackTrace();
            }

        }

    }
    private  boolean removeDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    removeDirectory(aFile);
                }
            }
          return  dir.delete();
        } else {
          return  dir.delete();
        }
    }

}
