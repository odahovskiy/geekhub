<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Вова
  Date: 05.12.2014
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="style/css/css.css">
</head>
<body>
  <form action="/index" method="post">
    <h3 style="color:aqua" align="center"><c:out value="${fileName}"/></h3>
    <textarea id="area" name="area" style="width: 100%; height: 80%"><c:out value="${file}" escapeXml="false"/></textarea>
    </br>
    <input type="hidden" name="url" value="${fileName}"/>
    <p align="center">
        <a href="${back_link}">back</a>
        <button id="buttonUpdate" type="submit" value="update" name="action">Update</button>
    </p>
  </form>
</body>
</html>