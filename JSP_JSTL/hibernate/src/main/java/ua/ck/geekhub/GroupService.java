package ua.ck.geekhub;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;

import java.util.List;

/**
 * Created by V.Odahovskiy.
 * Date 12.02.2015
 * Time 11:30
 * Package ua.ck.geekhub
 */
@Repository
@Transactional
public class GroupService {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Group group) {
        sessionFactory.getCurrentSession().saveOrUpdate(group);
    }

    public Group findByIdUnique(int id) {
        Group group = (Group) sessionFactory.getCurrentSession().get(Group.class, id);
        if (null == group) return null;
        group.setUsers(findMembers(group));
        return group;
    }

    public List<Group> findAll(){
        List<Group> groups = sessionFactory.getCurrentSession().createCriteria(Group.class).list();
        for(Group group : groups) group.setUsers(findMembers(group));
        return groups;
    }

    private List<User> findMembers(Group group){
       return sessionFactory.getCurrentSession().createQuery("from User u where u.group.id = :g_id")
                .setParameter("g_id",group.getId()).list();
    }

    public void delete(Group group){
        if (null != group) {
            if (null != findByIdUnique(group.getId())) {
                sessionFactory.getCurrentSession().delete(group);
            }
        }
    }

}
