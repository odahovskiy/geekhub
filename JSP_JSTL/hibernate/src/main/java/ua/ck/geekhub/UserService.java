package ua.ck.geekhub;

import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author vladimirb
 * @since 3/11/14
 */
@Repository
@Transactional
public class UserService {

	@Autowired
	SessionFactory sessionFactory;

	public void saveUser(User user) {

        if (null != user ){
            if (null == getUser(user.getId())) {
                sessionFactory.getCurrentSession().save(user);
            }else {
                sessionFactory.getCurrentSession().merge(user);
            }
        }

	}

	public User getUser(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public List<User> getUsers() {
		return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
	}

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

	public void createUser(String firstName, String lastName, String email) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        saveUser(user);
    }

    public User getUserH(Integer userId) {
        String hql = "select user from User user where user.id = :userId";
        return  (User) sessionFactory.getCurrentSession().createQuery(hql)
                .setInteger("userId", userId).uniqueResult();
    }


}
