<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Вова
  Date: 12.02.2015
  Time: 12:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    
    <form:form modelAttribute="user" commandName="user" action="/user/update" method="post">
        <select  hidden="hidden" name="userId"><option value="${user.id}" selected="true"/></select>
        First Name:<form:input path="firstName"/>
        Last Name:<form:input path="lastName"/>
        Email:<form:input  path="email"/>
        Group:
        <select name="groupId">
            <c:forEach var="group" items="${groups}">
                <c:choose>
                    <c:when test="${group.id eq user.group.id}">
                        <option value="${group.id}" selected="true">${group.name}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${group.id}">${group.name}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <button type="submit">UPDATE</button>
    </form:form>
</body>
</html>
