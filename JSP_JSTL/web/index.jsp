<%--
  Created by IntelliJ IDEA.
  User: Вова
  Date: 07.12.2014
  Time: 23:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title></title>
    <style>
      body{background: dimgray}
      td p{
        font-weight: bold;
      }
    </style>
  </head>
  <body>
    <table border="1px" align="center" bgcolor="#008b8b">
      <tr>
        <td><p align="center">Name</p></td>
        <td><p align="center">Value</p></td>
        <td><p align="center">Action</p></td>
      </tr>
      <c:forEach var="item" items="${sessionScope}">
        <tr>
          <td>${item.key}</td>
          <td>${item.value}</td>
          <td><a href="?name=${item.key}&action=delete">delete</a></td>
        </tr>
      </c:forEach>
      <tr>
          <form method="get">
            <td>
              <input type="text" name="name">
            </td>
            <td>
              <input type="text" name="value">
            </td>
            <td>
              <input type="submit" name="action" value="add">
            </td>
          </form>
      </tr>
    </table>
  </body>
</html>
