package com.odahovskiy.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

/**
 * Created by Vova Odahovskiy on 07.12.2014.
 */
@WebServlet("")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");
        String name = req.getParameter("name");
        String value = req.getParameter("value");
        HttpSession session = req.getSession();

        doAction(action, name, value, session);

        req.getRequestDispatcher("/index.jsp").forward(req,resp);
    }

    private void doAction(String action, String name, String value, HttpSession session) {
        if ( ( null != action ) || ( null != name ) ) {
            action = action.toLowerCase();
            switch (action) {
                case "add":
                    session.setAttribute(name, value);
                    break;
                case "delete":
                    session.removeAttribute(name);
                    break;
                default:
                    break;
            }
        }
    }
}
