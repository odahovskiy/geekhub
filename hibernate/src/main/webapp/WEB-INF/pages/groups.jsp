<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Вова
  Date: 12.02.2015
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

</head>
<body>
    <table border="1">
        <tr>
            <td>ID</td>
            <td>GROUP NAME</td>
            <td>MEMBERS</td>
        </tr>

        <c:forEach var="group" items="${groups}">
            <tr>
                <td>${group.id}</td>
                <td>${group.name}</td>
                <td>${group.users.size()}</td>
                <td>
                    <form action="/group${group.id}/delete" method="POST">
                        <button type="submit">delete</button>
                    </form>
                </td>
                <td>
                    <form id="rename-group-form"action="/group/update" method="post">
                        <select hidden="hidden" name="groupId"><option value="${group.id}" selected="selected"/></select>
                        <input hidden="hidden" name="group-name"/>
                        <button type="submit" id="rename-btn" onclick="var rename = function () {
                            var name = prompt('Input new group name:');
                            document.forms['rename-group-form'].elements['group-name'].value=name;
                        };
                        rename()">rename</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>

    </br></br>

    <table border="1">
        <c:forEach var="group" items="${groups}">
            <c:if test="${group.users.size() gt  0}">
                <tr>
                    <td>
                        <b>${group.name}</b>
                    </td>
                </tr>
                <tr>
                    <td>ID</td>
                    <td>FIRST NAME</td>
                    <td>LAST NAME</td>
                    <td>EMAIL</td>
                </tr>
                <c:forEach var="user" items="${group.users}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.email}</td>
                        <td>
                            <p>

                                <form:form method="POST" id="leave" action="/user${user.id}/leaveGroup">
                                    <c:if test="${not empty user.group}">
                                        <a onclick="document.forms['leave'].submit()">(<span style="color:red">dismiss</span>)</a>
                                    </c:if>
                                </form:form>

                            </p>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </c:forEach>
    </table>
</body>
</html>
