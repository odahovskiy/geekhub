package main.java.ua.ck.geekhub;

import org.springframework.web.bind.annotation.*;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class HelloController {

    @Autowired
	UserService userService;

    @Autowired
    private GroupService groupService;

	@RequestMapping(value = "/", method = {RequestMethod.GET,RequestMethod.HEAD})
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "redirect:users";
	}

	@RequestMapping(value = "/users", method = {RequestMethod.GET,RequestMethod.HEAD})
	public String user(ModelMap model) {
		model.addAttribute("users", userService.getUsers());
        model.addAttribute("groups", groupService.findAll());
        return "hello";
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(
			@RequestParam String firstName,
	        @RequestParam String lastName,
	        @RequestParam String email,
            @RequestParam Integer groupId
	) {
        userService.saveUser(new User(firstName,lastName,email,groupService.findByIdUnique(groupId)));
        return "redirect:users";
    }

    /**
     * View user edit page
     * @param id identification of user
     * @return edit page or redirect to user list if not find user by id
     */
    @RequestMapping(value = "/user{id}/edit",method = RequestMethod.GET)
    public String editUser(@PathVariable("id")Integer id,ModelMap model){
        if (null != id){
            User user = userService.getUser(id);
            model.addAttribute("user", user);
            model.addAttribute("groups", groupService.findAll());
            return "edit-user";
        }
        return "redirect:/users";
    }

    /**
     * Remove user from storage
     * @param id identification of user
     */
    @RequestMapping(value = "/user{id}/delete",method = RequestMethod.POST)
    public String editUser(@PathVariable("id")Integer id){
        if (null != id){
            User user = userService.getUser(id);
            userService.deleteUser(user);
        }
        return "redirect:/users";
    }

    /**
     * Update user data
     * @param user object to update
     * @param groupId id of selected group by user
     * @return view user list
     */
    @RequestMapping(value = "/user/update",method = RequestMethod.POST)
    public String editUser(@ModelAttribute("user")User user,
                           @RequestParam(value = "groupId",required = true)Integer groupId,
                            @RequestParam(value = "userId",required = true)String userId){
        if (null != user){
            if (null != groupId) {
                Group group = groupService.findByIdUnique(groupId);
                user.setGroup(group);
            }
            user.setId(Integer.valueOf(userId));
            userService.saveUser(user);
        }

        return "redirect:/users";
    }

    /**
     * Remove group from user
     * @param id identification of user
     * @return user list view
     */
    @RequestMapping(value = "/user{id}/leaveGroup",method = RequestMethod.POST)
    public String leaveGroup(@PathVariable("id")Integer id) {
        if (null != id) {
            User user = userService.getUser(id);
            user.setGroup(null);
            userService.saveUser(user);
        }
        return "redirect:/users";
    }

    @RequestMapping(value = "/group/update",method = RequestMethod.POST)
    public String updateGroup(@RequestParam("groupId")Integer groupId,
                              @RequestParam("group-name")String groupName) {

        Group group = groupService.findByIdUnique(groupId);
        group.setId(groupId);
        group.setName(groupName);
        System.out.println(group.getId() +" "+group.getName());
        //groupService.save(group);

        return "redirect:/groups";
    }

    @RequestMapping(value = "/group{id}/delete",method = RequestMethod.POST)
    public String deleteGroup(@PathVariable("id")Integer id){
        groupService.delete(groupService.findByIdUnique(id));
        return "redirect:/groups";
    }
    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public String groups(ModelMap model) {
        model.addAttribute("groups", groupService.findAll());
        return "groups";
    }
}