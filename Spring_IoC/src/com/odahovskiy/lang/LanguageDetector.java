package com.odahovskiy.lang;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class LanguageDetector {

	private Map<Language, Set<String>> mapping;
	private final String delimiter = "[^a-zA-Zа-яА-ЯёЁъЪ]+";


	public Language detectLanguage(String text) {
		if (mapping == null) {
			initMapping();
		}
		final Map<Language,Integer> matchers = new HashMap<>();
		List<String> words = new LinkedList<>(Arrays.asList(text.split(delimiter)));

		for (Language language : mapping.keySet()) {
			int before = words.size();
			words.removeAll(mapping.get(language));
			int after = words.size();
			matchers.putIfAbsent(language, (before - after));
		}

		Language translateLang = Language.ENGLISH;
		try {
			translateLang = matchers.entrySet()
					.stream()
					.max(Map.Entry.comparingByValue(Integer::compareTo))
					.get().getKey();

		}catch (NoSuchElementException ex){return translateLang;}

		//if no any word to translate return English
		return (0 != matchers.get(translateLang))? translateLang
												 : Language.ENGLISH;
	}

	public void setMapping(Map<Language, Set<String>> mapping) {
		this.mapping = mapping;
	}

	private void initMapping() {
		mapping = new HashMap<>();
	}
}
