package com.odahovskiy.main;


import com.odahovskiy.lang.LanguageDetector;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

public class Controller {

	public static void main(String[] args) throws BeansException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/odahovskiy/config.xml");

        Translator translator = applicationContext.getBean(Translator.class);
        String translation = translator.translate("d:/1.txt");
		System.out.println(translation);
    }
}
