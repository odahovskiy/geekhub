package com.odahovskiy.main;

import com.odahovskiy.lang.Language;
import com.odahovskiy.lang.LanguageDetector;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileNotFoundException;
import java.util.*;

public class Translator {

	private Set<Language> languages;

	private TextSource textSource;

	private Dictionary dictionary;

	private LanguageDetector languageDetector;

	public Translator() {
		/*languages = new HashSet<>(Arrays.asList(Language.values()));
		languageDetector.setMapping(getAllAvaliableKeyWords());*/
	}

	public String translate(String source) {
		String text = textSource.getText(source);
		Language language = languageDetector.detectLanguage(text);
		String[] words = text.split(" ");
		StringBuilder sb = new StringBuilder();
		for (String word : words) {
			String translatedWord = dictionary.translate(word, language);
			sb.append(translatedWord + " ");
		}
		return sb.toString();
	}

	private Map<Language, Set<String>> getAllAvaliableKeyWords() {
		Map<Language, Set<String>> availableWords = new HashMap<>(Collections.EMPTY_MAP);

		for (Language language : languages) {
			try {
				availableWords.put(language, dictionary.getAvaliableKeyWords(language));
			}catch (Exception ex){
				System.out.println(ex.getMessage());
			}
		}

		return availableWords;
	}


    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public TextSource getTextSource() {
        return textSource;
    }

    public void setTextSource(TextSource textSource) {
        this.textSource = textSource;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public LanguageDetector getLanguageDetector() {
        return languageDetector;
    }

    public void setLanguageDetector(LanguageDetector languageDetector) {
        this.languageDetector = languageDetector;
    }
}
