package com.odahovskiy.main;

import java.util.List;

public class TextSource {

	public String getText(String path) {
		List<String> load = ResourceLoader.getInstance().load(path);
		StringBuilder sb = new StringBuilder();
		for (String s : load) {
			sb.append(s);
		}
		return sb.toString();
	}
}
