package com.odahovskiy.main;


import com.odahovskiy.lang.Language;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dictionary {
	private Map<Language, Map<String, String>> dictionaries = new HashMap<Language, Map<String, String>>();

	//dont translate first word
	public String translate(String word, Language language) {
		Map<String, String> dictionary = getDictionary(language);
		String translation = dictionary.get(word);
		return translation == null ? word : translation;
	}

	public Set<String> getAvaliableKeyWords(Language language){
		Map<String, String> dictionary = getDictionary(language);
		return dictionary.keySet();
	}

	private Map<String, String> getDictionary(Language language) {
		Map<String, String> dictionary = dictionaries.get(language);
		if (null == dictionary) {
			dictionary = loadDictionary(language);
			dictionaries.put(language, dictionary);
		}
		return dictionary;
	}

	private Map<String, String> loadDictionary(Language language) {
		Map<String, String> dict = new HashMap<String, String>();
		List<String> lines = ResourceLoader.getInstance().load("Spring_IoC/dict/" + language.name().toLowerCase() + ".dict");
		for (String line : lines) {
			String[] parts = line.split("=");
			dict.put(parts[0], parts[1]);
		}
		return dict;
	}
}
